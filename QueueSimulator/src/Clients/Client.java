package Clients;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

public class Client {

	public int ID;
	public int tArrival;
	public int tService;
	
	public Client(int ID, int tArrival, int tService)
	{
		this.ID = ID;
		this.tArrival = tArrival;
		this.tService = tService;
	}
	
	
	public void decrTService()
	{
		this.tService--;
	}
	
	private static int getRandomNr(int min, int max)
	{
		if (min >= max)
		{
			throw new IllegalArgumentException("max must be > than");
		}
		
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
		
		
	}
	
	public static Queue<Client> genRandomClients(int nrOfClients, int tArrMin, int tArrMax, int tSerMin, int tSerMax)
	{
		Queue<Client> clients = new LinkedList<Client>(); 
		for(int i = 0; i < nrOfClients; i++)
		{
			
			Client client = new Client(i + 1, getRandomNr(tArrMin, tArrMax), getRandomNr(tSerMin, tSerMax) );
			clients.add(client);
			
		}
		
		
		return clients;
	}
	
	
	public int getID()
	{
		return ID;
	}
	
	public int getTArrival()
	{
		return tArrival;
	}
	
	public int getTService()
	{
		return tService;
	}
	
	public void setID(int ID)
	{
		this.ID = ID;
	}
	
	public void setTArrival(int tArrival)
	{
		this.tArrival = tArrival;
	}
	
	public void setTService(int tService)
	{
		this.tService = tService; 
	}
	
	public void printClient() 
	{
		
		System.out.print("(" + getID()+ "," + getTArrival() + "," + getTService() + "); ");
	}
	

	public static void main(String[] args)
	{
		
		Client client1 = new Client(5, 18, 7);
		
		
		
		System.out.print("Client with the parameters 5, 18, 7 was created, this is the client : ");
		client1.printClient();
		System.out.println();
		System.out.println();
		System.out.println();
		
		System.out.print("The client's tService time has been decreased by 1, here is the client : ");
		client1.printClient();
		System.out.println();
		System.out.println();
		System.out.println();
		
		System.out.println("Here is a random number between 10 and 100 : " + getRandomNr(10, 100));
		
		
		
		
		
	}
	
	
}

	
	
