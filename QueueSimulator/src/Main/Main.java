package Main;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

import ClientQueue.ArrayOfQueues;
import ClientQueue.ClientQueue;
import Clients.*;


public class Main {	
	
	
	
	public static void main(String[] args) throws IOException {

			String s = new String();
			Scanner scanner = new Scanner(new File("in.txt"));
			while(scanner.hasNextLine())
			{
				s += " ";
				s += scanner.nextLine();
				s.split(",");
			}
	
			String[] val = s.split("\\s|\\,");		
			int N = Integer.parseInt(val[1]);
			int Q = Integer.parseInt(val[2]);
			int tSim = Integer.parseInt(val[3]);
			int tArrMin = Integer.parseInt(val[4]);
			int tArrMax = Integer.parseInt(val[5]);
			int tSerMin = Integer.parseInt(val[6]);
			int tSerMax = Integer.parseInt(val[7]);
			
			PrintStream out = new PrintStream(new FileOutputStream("out.txt"));
			System.setOut(out);
			

			ArrayOfQueues arrayQ = new ArrayOfQueues(tSim);
			arrayQ.createQQueues(Q);
			Queue<ClientQueue> queueOfClients = new LinkedList<ClientQueue>();
			ArrayList<Thread> threads = new ArrayList<Thread>();
			Queue<Client> clients = Client.genRandomClients(N, tArrMin, tArrMax, tSerMin, tSerMax);
			
			arrayQ.initThreads();
			
			int avgTime = 0;
			int size = clients.size();
			
			for(Client x : clients)
			{
				avgTime += x.getTService();
			}
			
			
			for(int i = 0; i <= tSim; i++)
			{

				System.out.println("Time: " + i);
				arrayQ.updateQWithServTime();
				arrayQ.updateQWithClients(clients, i);
				System.out.print("Waiting clients: ");
				for(Client x : clients)
				{
					x.printClient();
				}
				System.out.println();
				arrayQ.printArrayOfQueues();			
				System.out.println();
				
			}
			
			System.out.println("Average waiting time: " + (double)avgTime/size);
			
			
			
		}

	
	}


