package ClientQueue;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

import Clients.Client;

public class ArrayOfQueues implements Runnable {
	
	private ArrayList<Thread> threads = new ArrayList<Thread>();
	private ArrayList<ClientQueue> queueOfClients = new ArrayList<ClientQueue>();
	private int simTime;
	
	public ArrayOfQueues(int simTime)
	{
		this.simTime = simTime;
		this.queueOfClients = new ArrayList<ClientQueue>();
	}
	
	public void initThreads()
	{
		for(int i = 0; i < this.queueOfClients.size(); i++)
		{
			threads.add(new Thread());
		}
	}
	public void createQQueues(int Q)
	{
		for(int i = 0; i < Q; i++)
		{
			queueOfClients.add(new ClientQueue());
			
		}
	}
	
	public void printArrayOfQueues()
	{
		
		for(ClientQueue x : queueOfClients)
		{
			System.out.print("Queue " + queueOfClients.indexOf(x) + ": ");
			x.printClientQueue();
			
		}
		
	}
	
	public ClientQueue getMinQ()
	{
		ClientQueue min = queueOfClients.get(0);
		if(min.chechEmpty() == true) {return min;}
		for(ClientQueue x : queueOfClients)
		{
			if(x.chechEmpty() == true) {return x;}
			if(min.getSTime() > x.getSTime())
			{
				min = x;
			}
		}
		return min;
	}
	
	public void updateQWithClients(Queue<Client> clients, int actualTime)
	{
		
		Queue<Client> q1 = clients;
		Iterator<Client> itr = q1.iterator();
		while(itr.hasNext()) {
			
			Client x = itr.next();
			ClientQueue temp = getMinQ();
			int index = this.queueOfClients.indexOf(temp);
			
		
		if(x.getTArrival() <= actualTime)
		{
			temp.addClientToQ(x);
			temp.initSTime();
			this.queueOfClients.set(index, temp);
			
			itr.remove();
			if(itr.hasNext())
			{
				x = itr.next();	
			}
			else
			{
				break;
			}
		}
		
		
		}
	}
	
	public void updateQWithServTime()
	{
		for(ClientQueue x : this.queueOfClients)
		{
			Thread t = threads.get(this.queueOfClients.indexOf(x));
			t.run();
			if(x.chechEmpty() == false)
			{
				x.decrSTime();
				x.initSTime();
				this.queueOfClients.set(this.queueOfClients.indexOf(x), x);
			}
			
			
		}
	}

	public void run() {
		
		try {
			
			for(ClientQueue x : this.queueOfClients)
			{
				if(x.chechEmpty() == true)
				{ 
					Thread t = threads.get(this.queueOfClients.indexOf(x));
					t.sleep(3600000);
					threads.set(threads.indexOf(t), t);
					
				}
				else
				{
					Thread t = threads.get(this.queueOfClients.indexOf(x));
					t.interrupt();
					threads.set(threads.indexOf(t), t);
				}
				
			}
			
		}catch(Exception e) {}
		
		
	}
	
	
	public static void main(String[] args)
	{
		ArrayOfQueues arrayQ = new ArrayOfQueues(20);
		arrayQ.createQQueues(5);
		Queue<ClientQueue> queueOfClients = new LinkedList<ClientQueue>();
		ArrayList<Thread> threads = new ArrayList<Thread>();
		Queue<Client> clients = Client.genRandomClients(10, 1, 5, 2, 4);

		System.out.println("Here are 5 Queues, which are empty : ");
		arrayQ.printArrayOfQueues();
	
		System.out.println();
		System.out.println();
		System.out.println();
		
		System.out.println("Here is a smaller simulation : ");
		
		arrayQ.initThreads();
		
		for(int i = 0; i <= 10; i++)
		{

			System.out.println("Time: " + i);
			arrayQ.updateQWithServTime();
			arrayQ.updateQWithClients(clients, i);
			System.out.print("Waiting clients: ");
			for(Client x : clients)
			{
				x.printClient();
			}
			System.out.println();
			arrayQ.printArrayOfQueues();			
			System.out.println();
			
		}
		
	}
	
	
	
}
