package ClientQueue;

import java.util.LinkedList;
import java.util.Queue;

import Clients.Client;

public class ClientQueue {
	
	
	private Queue<Client> queueOfClients = new LinkedList<Client>();
	private int totalSTime = 0;
	
	public ClientQueue()
	{
		this.queueOfClients = new LinkedList<Client>();
		this.totalSTime = 0;
	}
	
	
	public  ClientQueue(Queue<Client> queueOfClients , int totalSTime) 
	{
		  this.queueOfClients = queueOfClients;
		  this.totalSTime = totalSTime;
	}
	
	public boolean chechEmpty()
	{
		if(queueOfClients.size() > 0)	return false;
		return true;
			
	}

	public Queue<Client> getQ()
	{
		return queueOfClients;
	}
	
	public void setQ(Queue<Client> q)
	{
		this.queueOfClients = q;
	}
	public void addClientToQ(Client client)
	{
		queueOfClients.add(client);
	}
	 
	public void initSTime()
	{
		for(Client x : queueOfClients)
		{
			totalSTime += x.getTService();
		}
		
		setSTime(totalSTime);
	}
	
	public void printClientQueue()
	{
		if(queueOfClients.isEmpty()) System.out.println("Closed");
		else {
		for(Client x : queueOfClients)
		{
			x.printClient();
		}
		System.out.println();
		}
	}
	
	public void setSTime(int totalSTime)
	{
		this.totalSTime = totalSTime;
	}
	
	public void addSTime(int sTime)
	{
		this.totalSTime += sTime;
	}
	
	public int getSTime()
	{
		return totalSTime;
	}
	
	public void decrSTime()
	{
		Client x = this.queueOfClients.peek();
		if(x.getTService() > 0)
			x.decrTService();
		if(x.getTService() == 0)
			this.queueOfClients.remove();
	}
	
	
	public static void main(String[] args)
	{
		
		Queue<Client> clients = Client.genRandomClients(7, 2, 30, 2, 30);
		ClientQueue clq = new ClientQueue(clients, 0);
		clq.initSTime();
		System.out.println("Here is a Queue of 7 random Clients : ");
		clq.printClientQueue();
		
		System.out.println();
		System.out.println();
		System.out.println();
		
		
		System.out.println("The totalSTime for the previous Queue is : " + clq.getSTime());
		
		System.out.println();
		System.out.println();
		System.out.println();
		
		clq.decrSTime();
		
		System.out.println("Now here is the Queue after decrSTime() was used ");
		clq.printClientQueue();
		
		System.out.println();
		System.out.println();
		System.out.println();
		
		ClientQueue etyQ = new ClientQueue();
		if(etyQ.chechEmpty() == true)
			{
				System.out.print("The if statement works => the Queue is empty, and the Queue is : ");
				etyQ.printClientQueue();
				
			}
		
		System.out.println();
		System.out.println();
		System.out.println();
		
		
		
		
		
		
		
		
		
	}
	
}
